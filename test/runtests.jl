using Xcb
using Test

const xcb = Xcb

scr = Ref{Cint}()
# connect to xserver
connection = xcb.xcb_connect(C_NULL, scr)
println(connection)
#
setup = xcb.xcb_get_setup(connection)
println(setup)
iter = xcb.xcb_setup_roots_iterator(setup)
screen = unsafe_load(iter.data, 1)

window = xcb.xcb_generate_id(connection)
mask = xcb.XCB_CW_BACK_PIXEL | xcb.XCB_CW_EVENT_MASK
value_list = zeros(UInt32, 32)
value_list[1] = screen.white_pixel
value_list[2] = xcb.XCB_EVENT_MASK_EXPOSURE | xcb.XCB_EVENT_MASK_KEY_PRESS

xcb.xcb_create_window(
        connection, UInt8(xcb.XCB_COPY_FROM_PARENT), window,
        screen.root, Int16(0), Int16(0), UInt16(512), UInt16(512), UInt16(1),
        UInt16(xcb.XCB_WINDOW_CLASS_INPUT_OUTPUT), screen.root_visual,
        mask, value_list
)
mask = xcb.XCB_GC_FOREGROUND | xcb.XCB_GC_GRAPHICS_EXPOSURES
value_list[1] = screen.black_pixel
value_list[2] = 0

g = xcb.xcb_generate_id(connection)
xcb.xcb_create_gc(connection, g, window, mask, value_list)

xcb.xcb_map_window(connection, window)
xcb.xcb_flush(connection)
r = Ref(xcb.xcb_rectangle_t(20,20,60,60))
# event loop
done = false
while !done
    e = unsafe_load(xcb.xcb_wait_for_event(connection))
    if e.response_type == xcb.XCB_EXPOSE
        println("EXPOSE")
        xcb.xcb_poly_fill_rectangle(connection, window, g, UInt32(1), r)
        xcb.xcb_flush(connection)
    elseif e.response_type == xcb.XCB_KEY_PRESS
        # exit on keypress */
        global done = true
    end
end
xcb.xcb_disconnect(connection)


@testset "xcb.jl" begin
    # Write your tests here.
end
