# Xcb.jl
This is a Julia wrapper for the xcb library. 
Compatibility tested only with Julia 1.5 but should work with Julia 1.5+.

## Dependencies
For this wrapper to work, xcb should be installed in your system.

## Adding Package
~~~julia
julia> using Pkg
julia> Pkg.add(url="https://gitlab.com/poposca/Xcb.jl.git")
~~~

## Example
~~~julia
julia> using xcb
~~~